import sys

base_trees = 0
fruit_per_tree = 0
reduction = 0

def compute_trees(trees):
    global base_trees, fruit_per_tree, reduction

    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production

def compute_all(min_trees, max_trees):
    global base_trees, fruit_per_tree, reduction
    productions = [(num_trees, compute_trees(num_trees)) for num_trees in range(min_trees, max_trees + 1)]
    return productions

def read_arguments():
    global base_trees, fruit_per_tree, reduction

    if len(sys.argv) != 6:
        print("Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>")
        sys.exit(1)

    try:
        # Convertir argumentos a enteros
        base_trees = int(sys.argv[1])
        fruit_per_tree = int(sys.argv[2])
        reduction = int(sys.argv[3])
        min_trees = int(sys.argv[4])
        max_trees = int(sys.argv[5])

        if not all(isinstance(arg, int) for arg in [base_trees, fruit_per_tree, reduction, min_trees, max_trees]):
            raise ValueError

        return base_trees, fruit_per_tree, reduction, min_trees, max_trees

    except ValueError:
        print("All arguments must be integers")
        sys.exit(1)

def main():
    global base_trees, fruit_per_tree, reduction

    base_trees, fruit_per_tree, reduction, min_trees, max_trees = read_arguments()
    productions = compute_all(min_trees, max_trees)

    best_production = max(productions, key=lambda x: x[1])
    for num_trees, production in productions:
        print(f"{num_trees} {production}")
    print(f"Best production: {best_production[1]}, for {best_production[0]} trees")

if __name__ == '__main__':
    main()
